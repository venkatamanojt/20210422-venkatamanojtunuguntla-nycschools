# 20210422-venkatamanojtunuguntla-nycschools

iOS application
Swift 5.0 and XCode 12.4

Project: This iOS application fetches all the list of NY schools from the data.cityofnewyork.us API.

Used MVVM Architecture. This design helps in keeping the view controller light.

Features:
*On launching the app, a list of all the NYC schools are fetched & displayed in Ascending order.
*Selecting a school, would navigate the user to the next screen where the different SAT average scores would be fetched & displayed.
*Safe Area Insets
*Portrait & Landscape supported.

Additions:
Accessibility-Voice Over for the schools list as well as the school average SAT scores screen.

Assumptions / Constraints:
-Only NYC highschools were fetched. 
-Even though NSURLSession is used to fetch the data, due to the time constraint, networking layer wasn't completely added. Have hardcoded the URL's & related methods. 

Scope for improvement:
-Implement proper network layer & error Handling
-Implement caching, so that the same API wouldn't be refetched multiple times.
-Add RefreshControl for TableView to refetch new data.
-Sort the list by Ascending/Descending.
-Search Functionality
-Rather than fetching all the data at once, add Pagination, using the offset & limit query parameters in the request.
-Unit Test Cases.
