//
//  ServiceManager.swift
//  20210422-venkatamanojtunuguntla-nycschools
//
//  Created by Venkata Manoj Tunuguntla on 4/22/21.
//

import Foundation

enum ServiceError: Error {
    case noData
    case invalidResponse
    case requestFailed
    case invalidData
}

class ServiceManager {
    
    typealias SchoolListCompletion = (SchoolInfoList?, ServiceError?) -> ()
    typealias SchoolStatisticsCompletion =
        (SchoolStatisticsList?, ServiceError?) -> ()
    
    private static let host = "data.cityofnewyork.us"
    private static let schoolsPath = "/resource/s3k6-pzi2.json"
    private static let statisticsPath = "/resource/f9bf-2cp4.json"
    
    /// To fetch the list of schools in NY location. It is assumed that only NY schools needs to be displayed, hence it is hardcoded to that. Also, only specific parameters were being fetched so as to reduce the payload size.
    ///
    /// - Parameter completion: closure
    static func fetchSchoolList(completion: @escaping SchoolListCompletion) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = host
        urlComponents.path = schoolsPath
        urlComponents.queryItems = [
            URLQueryItem(name: "state_code", value: "NY"),
            URLQueryItem(name: "$order", value: "school_name ASC"),
            URLQueryItem(name: "$select", value: "school_name,dbn,website")
        ]
        let url = urlComponents.url!
        
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            //execute completion handler on main thread
            DispatchQueue.main.async {
                if error != nil {
                    print("Request Failed: \(error!.localizedDescription)")
                    completion(nil, .requestFailed)
                    return
                }
                
                if let response = response as? HTTPURLResponse {
                    
                    switch response.statusCode {
                    case 200...299:
                        guard let responseData = data else {
                            print("Error getting data")
                            completion(nil, .noData)
                            return
                        }
                        do {
                            if let jsonData = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) {
                                print(jsonData)
                                let schoolInfoListResponse = try JSONDecoder().decode(SchoolInfoList.self, from: responseData)
                                print(schoolInfoListResponse)
                                completion(schoolInfoListResponse, nil)
                            }
                        }catch {
                            print(error)
                            completion(nil, .invalidData)
                        }
                    default:
                        completion(nil, .invalidResponse)
                        return
                    }
                }
            }
        }
        dataTask.resume()
    }
    
    /// API to fetch the schools statistics from the selected school.
    ///
    /// - Parameters:
    ///   - dbnSchoolInfo: selected school
    ///   - completion: closure
    static func fetchSchoolStatistics(for dbnSchoolInfo:String, completion: @escaping SchoolStatisticsCompletion){
        
       // let urlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbnSchoolInfo)"

        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = host
        urlComponents.path = statisticsPath
        urlComponents.queryItems = [
            URLQueryItem(name: "dbn", value: dbnSchoolInfo)
        ]
        let url = urlComponents.url!
        
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            //execute completion handler on main thread
            DispatchQueue.main.async {
                if error != nil {
                    print("Request Failed: \(error!.localizedDescription)")
                    completion(nil, .requestFailed)
                    return
                }
                
                if let response = response as? HTTPURLResponse {
                    switch response.statusCode {
                    case 200...299:
                        guard let responseData = data else {
                            print("Error getting data")
                            completion(nil, .noData)
                            return
                        }
                        do {
                            if let jsonData = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) {
                                print(jsonData)
                                let schoolStatisticsListResponse = try JSONDecoder().decode(SchoolStatisticsList.self, from: responseData)
                                print(schoolStatisticsListResponse)
                                completion(schoolStatisticsListResponse, nil)
                            }
                        }catch {
                            print(error)
                            completion(nil, .invalidData)
                        }
                    default:
                        completion(nil, .invalidResponse)
                        return
                    }
                }
            }
        }
        dataTask.resume()
    }
}
