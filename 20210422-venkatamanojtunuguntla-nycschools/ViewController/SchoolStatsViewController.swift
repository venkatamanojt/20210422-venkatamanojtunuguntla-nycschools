//
//  SchoolStatsViewController.swift
//  20210422-venkatamanojtunuguntla-nycschools
//
//  Created by Venkata Manoj Tunuguntla on 4/22/21.
//

import UIKit

class SchoolStatsViewController: UIViewController {

    var schoolStatisticsListViewModel: SchoolStatisticsViewModel = SchoolStatisticsViewModel()
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var readingAvgScoreLabel: UILabel!
    @IBOutlet weak var noOfTestTakers: UILabel!
    @IBOutlet weak var writingAvgScoreLabel: UILabel!
    @IBOutlet weak var mathAvgScoreLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initializeViewModel()
    }
    
    /// To set up the view model
    private func initializeViewModel() {
        
        self.title = "SAT Average Scores"
        guard let selectedSchool = schoolStatisticsListViewModel.selectedSchoolInfo else {
            return
        }
        //update the data
        schoolStatisticsListViewModel.fetchSchoolStatisticsList(for: selectedSchool) {
            DispatchQueue.main.async { [weak self] in
                if let name = self?.schoolStatisticsListViewModel.schoolName{
                    self?.schoolNameLabel.text = name
                }else{
                    self?.schoolNameLabel.text = selectedSchool.schoolName
                }
                
                self?.readingAvgScoreLabel.text = self?.schoolStatisticsListViewModel.satCriticalReadingAvgScore
                
                self?.writingAvgScoreLabel.text = self?.schoolStatisticsListViewModel.satWritingAvgScore
                
                self?.mathAvgScoreLabel.text = self?.schoolStatisticsListViewModel.satMathAvgScore
                self?.noOfTestTakers.text = self?.schoolStatisticsListViewModel.noOfTestTakers
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
