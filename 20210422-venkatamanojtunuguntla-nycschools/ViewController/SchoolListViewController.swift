//
//  SchoolListViewController.swift
//  20210422-venkatamanojtunuguntla-nycschools
//
//  Created by Venkata Manoj Tunuguntla on 4/22/21.
//

import UIKit
/// View controller to display the list of schools in NY location
class SchoolListViewController: UIViewController {

    /// Table view
    @IBOutlet weak var schoolListTableView: UITableView!
    @IBOutlet weak var schoolListActivityIndicatorView: UIActivityIndicatorView!
    //View Model to fetch the data & also to do the processing
    private let schoolListViewModel = SchoolListViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initializeViewModel()
    }
    
    /// To set up the view model
    private func initializeViewModel() {
        schoolListTableView.rowHeight = UITableView.automaticDimension
        schoolListTableView.estimatedRowHeight = 44
        schoolListTableView.isAccessibilityElement = false
        schoolListTableView.shouldGroupAccessibilityChildren = true
        schoolListActivityIndicatorView.isHidden = false
        schoolListActivityIndicatorView.startAnimating()
        //Update the data in the viewcontroller
        schoolListViewModel.fetchSchoolsInfoList { [weak self] in
            self?.schoolListActivityIndicatorView.isHidden = true
            self?.schoolListActivityIndicatorView.stopAnimating()
            self?.schoolListTableView.reloadData()
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "SchoolStatisticsViewSegue" {
            performSegueForSchoolStatisticsViewController(segue: segue)
        }
    }
    
    /// Before navigating to the school statistics view, update the parent view model
    ///
    /// - Parameter segue: identifier
    private func performSegueForSchoolStatisticsViewController(segue :UIStoryboardSegue) {
        
        let schoolStatisticsVC = segue.destination as! SchoolStatsViewController
        schoolStatisticsVC.schoolStatisticsListViewModel.schoolListViewModel = self.schoolListViewModel
    }
}

// MARK: - UItableviewDataSource
extension SchoolListViewController :UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolListViewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolInfoTableViewCell", for: indexPath) as? SchoolInfoTableViewCell else {
            fatalError("Cell could not be initialized")
        }
        cell.schoolName.text = schoolListViewModel.schoolName(at: indexPath.row)
        cell.isAccessibilityElement = true
        cell.accessibilityLabel = schoolListViewModel.schoolName(at: indexPath.row)
        cell.accessibilityHint = "Click to see SAT Statistics"
        
        return cell
    }
}

// MARK: - UItableviewDelegate
extension SchoolListViewController :UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        schoolListViewModel.didSelectItem(at: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
