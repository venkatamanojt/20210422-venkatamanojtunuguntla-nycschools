//
//  SchoolInfoTableViewCell.swift
//  20210422-venkatamanojtunuguntla-nycschools
//
//  Created by Venkata Manoj Tunuguntla on 4/22/21.
//

import UIKit


/// School Info Table view cell
class SchoolInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var website: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
