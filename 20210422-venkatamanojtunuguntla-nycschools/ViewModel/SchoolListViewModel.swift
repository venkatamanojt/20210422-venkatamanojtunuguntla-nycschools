//
//  SchoolListViewModel.swift
//  20210422-venkatamanojtunuguntla-nycschools
//
//  Created by Venkata Manoj Tunuguntla on 4/22/21.
//

import Foundation

/// View Model to fetch the list of schools & parse the data (using model classes) & update the data in the ViewController
public class SchoolListViewModel {
    //The fetched list of schools is stored in this private variable.
    private var schoolList:SchoolInfoList = SchoolInfoList()
    
    //To store the selected school info when user selects a school
    private var selectedSchool:SchoolInfo?
    
    //use the selected school info while navigating to the statistics view
    var selectedSchoolInfo: SchoolInfo?{
        
        return selectedSchool ?? nil
    }
    
    /// Selected school name
    var selectedSchoolName: String {
        return selectedSchool?.schoolName ?? ""
    }
    
    
    /// To fetch the list of schools in NY location. It is assumed that only schools in NYC region are to be shown, hence the filter is currently set to NY. Also, the response parameters were choosen as per the requirement to display the list of schools. So to reduce the payload size, the request is set to fetch only the basic param information.
    ///
    /// - Parameter completion: closure to update the uitableview
    func fetchSchoolsInfoList(completion: @escaping () -> Void) {
        
        ServiceManager.fetchSchoolList {[weak self] (schoolList, error) in
            
            guard let self = self, let schoolListObj = schoolList else{
                return
            }
            DispatchQueue.main.async { [weak self] in
                self?.schoolList = schoolListObj
                completion()
            }
        }
    }
    
    //To provide the number of rows to be displayed in tableview
    func numberOfRows(in section: Int) -> Int {
        return self.schoolList.count
    }
    
    //to return the school Name at a particular index while rendering the tableview Cell
    func schoolName(at index: Int) -> String {
        return self.schoolList[index].schoolName
    }
    
    //When user selects a cell, before navigating to the detailed statistics screen, save the selected school info, so that it can be used while fetching the statistics information.
    func didSelectItem(at index: Int){
        self.selectedSchool = self.schoolList[index]
    }
}
