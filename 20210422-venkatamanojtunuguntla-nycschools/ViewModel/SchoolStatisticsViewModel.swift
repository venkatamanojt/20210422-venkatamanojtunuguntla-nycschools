//
//  SchoolStatisticsViewModel.swift
//  20210422-venkatamanojtunuguntla-nycschools
//
//  Created by Venkata Manoj Tunuguntla on 4/22/21.
//

import Foundation

/// View Model to fetch the selected school statistics & parse the data (using model classes) & update the data in the ViewController
public class SchoolStatisticsViewModel {
    //The fetched school statistics is stored in this private variable.
    private var schoolStatisticsList:SchoolStatisticsList = SchoolStatisticsList()
    var schoolListViewModel: SchoolListViewModel?
    
    //use the selected school info to fetch the school stats
    var selectedSchoolInfo: SchoolInfo?{
        return schoolListViewModel?.selectedSchoolInfo ?? nil
    }
    
    //score to be rendered in the view controller
    var satCriticalReadingAvgScore: String{
        if let readingAvgScore = schoolStatisticsList.first?.satCriticalReadingAvgScore, !readingAvgScore.isEmpty {
            return readingAvgScore
        }
        return "-NA-"
    }
    
    //score to be rendered in the view controller
    var satMathAvgScore: String {
        if let mathAvgScore = schoolStatisticsList.first?.satMathAvgScore, !mathAvgScore.isEmpty {
            return mathAvgScore
        }
        return "-NA-"
    }
    
    //score to be rendered in the view controller
    var satWritingAvgScore: String {
        if let writingAvgScore = schoolStatisticsList.first?.satWritingAvgScore, !writingAvgScore.isEmpty {
            return writingAvgScore
        }
        return "-NA-"
    }
    
    //score to be rendered in the view controller
    var noOfTestTakers: String {
        if let noOfTestTakersCnt = schoolStatisticsList.first?.noOfTestTakers, !noOfTestTakersCnt.isEmpty {
            return noOfTestTakersCnt
        }
        return "-NA-"
    }
    
    //score to be rendered in the view controller
    var schoolName: String? {
        if let name = schoolStatisticsList.first?.schoolName, !name.isEmpty {
            return name
        }
        return nil
    }
    
    /// To fetch the selected school statistics information.
    ///
    /// - Parameters:
    ///   - selectedSchool: selected School Info
    ///   - completion: closure to update the view controller
    func fetchSchoolStatisticsList(for selectedSchool: SchoolInfo, completion: @escaping () -> Void) {
         ServiceManager.fetchSchoolStatistics(for: selectedSchool.dbn) {[weak self] (schoolStatisticsList, error) in
            guard let self = self, let schoolStatisticsListObj = schoolStatisticsList else{
                return
            }
            DispatchQueue.main.async { [weak self] in
                self?.schoolStatisticsList = schoolStatisticsListObj
                completion()
            }
        }
    }
}
