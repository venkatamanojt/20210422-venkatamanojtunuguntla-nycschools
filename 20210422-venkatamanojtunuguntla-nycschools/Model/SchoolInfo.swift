//
//  SchoolInfo.swift
//  20210422-venkatamanojtunuguntla-nycschools
//
//  Created by Venkata Manoj Tunuguntla on 4/22/21.
//

import Foundation

typealias SchoolInfoList = [SchoolInfo]

struct SchoolInfo {
    let dbn: String
    let schoolName: String
    let website: String
}

extension SchoolInfo: Decodable {
    enum CodingKeys: String, CodingKey {
        case dbn        = "dbn"
        case schoolName = "school_name"
        case website    = "website"
    }
    
    init(from decoder: Decoder) throws {
        let schoolInfoContainer = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try schoolInfoContainer.decode(String.self, forKey: .dbn)
        schoolName = try schoolInfoContainer.decode(String.self, forKey: .schoolName)
        website = try schoolInfoContainer.decode(String.self, forKey: .website)
    }
}

extension SchoolInfo: CustomStringConvertible {
    var description: String {
        return "schoolInfoDBN: \(dbn) \n schoolName: \(schoolName) \n website: \(website)"
    }
}
