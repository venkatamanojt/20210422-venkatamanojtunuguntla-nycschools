//
//  SchoolStatistics.swift
//  20210422-venkatamanojtunuguntla-nycschools
//
//  Created by Venkata Manoj Tunuguntla on 4/22/21.
//

import Foundation

typealias SchoolStatisticsList = [SchoolStatistics]

struct SchoolStatistics {
    let dbn : String
    let satCriticalReadingAvgScore:String
    let satMathAvgScore: String
    let satWritingAvgScore: String
    let schoolName: String
    let noOfTestTakers: String
}

extension SchoolStatistics : Decodable {
    
    enum CodingKeys: String, CodingKey {
        case dbn                        = "dbn"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore            = "sat_math_avg_score"
        case satWritingAvgScore         = "sat_writing_avg_score"
        case schoolName                 = "school_name"
        case noOfTestTakers             = "num_of_sat_test_takers"
    }
    
    init(from decoder: Decoder) throws {
        let schoolStatisticsContainer = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try schoolStatisticsContainer.decode(String.self, forKey: .dbn)
        satCriticalReadingAvgScore = try schoolStatisticsContainer.decode(String.self, forKey: .satCriticalReadingAvgScore)
        satMathAvgScore = try schoolStatisticsContainer.decode(String.self, forKey: .satMathAvgScore)
        satWritingAvgScore = try schoolStatisticsContainer.decode(String.self, forKey: .satWritingAvgScore)
        schoolName = try schoolStatisticsContainer.decode(String.self, forKey: .schoolName)
        noOfTestTakers = try schoolStatisticsContainer.decode(String.self, forKey: .noOfTestTakers)
    }
}

extension SchoolStatistics: CustomStringConvertible {
    var description: String {
        return "schoolStatisticsDBN: \(dbn) \nSchoolName: \(schoolName) \n SAT Reading Score: \(satCriticalReadingAvgScore) \n SAT Math Average Score: \(satMathAvgScore) \n SAT Writing Average Score : \(satWritingAvgScore) \n No Of SAT Test Takers : \(noOfTestTakers)"
    }
}
